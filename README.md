Codeberg HTML Preview
-------------------------------

Many Codeberg repositories don't use Codeberg Pages to host their HTML files. **Codeberg HTML Preview** allows you to render those files without cloning or downloading whole repositories. It is a client-side solution using a CORS proxy to fetch assets.

If you try to open raw version of any HTML, CSS or JS file in a web browser directly from Codeberg, all you will see is a source code. Codeberg forces them to use the "text/plain" content-type, so they cannot be interpreted. This script overrides it by using a CORS proxy.

## Usage

In order to use it, just prepend this fragment to the URL of any HTML file: **[http://htmlpreview.codeberg.eu/?](http://htmlpreview.codeberg.eu/?)** e.g.:

 - http://htmlpreview.codeberg.eu/?https://codeberg.org/docs/pages/raw/branch/main/index.html

What it does is: load HTML using CORS proxy, then process all links, frames, scripts and styles, and load each of them using CORS proxy, so they can be evaluated by the browser.

## License

&copy; 2019 Jerzy Głowacki under Apache License 2.0.
